Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'pages/welcome'

  resources :articles  do
    resources :comments
  end

  root 'pages#welcome'
  get '/about', to: 'pages#about'
end
